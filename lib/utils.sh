root="$(cd "$(dirname "$0")/.." || exit ; pwd -P)"

PLUGIN_NAME="$(basename ${root})"
BASE_URL="https://storage.googleapis.com/gdk-assets/asdf-prebuilt"

if [[ "${PLUGIN_NAME}" == "prebuilt" ]]; then
	echo "ERROR: This plugin has been incorrectly added, aborting."
	exit 1
fi

base_url() {
	local download_url=""
}

get_timestamp() {
  date '+%s'
}

get_platform() {
  uname | tr '[:upper:]' '[:lower:]'
}

get_release() {
	local system_version

	if [[ "${OSTYPE}" == "linux"* ]]; then
		system_version=$(echo "$(lsb_release -s -i)_$(lsb_release -s -r)" | tr A-Z a-z)

		case ${system_version} in
			debian_11*)
				echo "bullseye"
				;;
			ubuntu_21.*|ubuntu_20.*)
				echo "bullseye"
				;;
			debian_9*|debian_10*)
				echo "stretch"
				;;
			ubunt_18.*)
				echo "stretch"
				;;
			*)
				echo "ERROR: Unknown/unsupported platform version." >&2
				exit 1
			;;
		esac
	elif [[ "${OSTYPE}" == "darwin"* ]]; then
		system_version=$(defaults read loginwindow SystemVersionStampAsString)

		case ${system_version} in
			12*)
				echo "monterey"
				;;
			11*)
				echo "big_sur"
				;;
			10.15*)
				echo "catalina"
				;;
			*)
				echo "ERROR: Unknown/unsupported platform version." >&2
				exit 1
			;;
		esac
	else
		echo "ERROR: Unknown/unsupported platform." >&2
		exit 1
	fi
}

get_arch() {
  uname -m
}
